package com.farms.util;

import com.google.common.net.MediaType;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.farms.util.CharacterType.LOWER_CASE;
import static com.farms.util.CharacterType.NUMERIC;
import static com.farms.util.CharacterType.SPECIAL;
import static com.farms.util.CharacterType.UPPER_CASE;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

public class Random {
    private static final SecureRandom RND = new SecureRandom();
    private static final String NUM = NUMERIC.getCharacters();
    private static final String ALPHA_NUM = NUMERIC.getCharacters() + UPPER_CASE.getCharacters() + LOWER_CASE.getCharacters();

    //ALPHA3 to ALPHA2 country code map
    private static final Map<String, String> COUNTRIES = Arrays.stream(Locale.getISOCountries()).map(
            c -> new AbstractMap.SimpleImmutableEntry<>(new Locale("",c).getISO3Country(), c))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

    public static int intVal() { return intVal(1000); }
    public static int intVal(final int max) { return max == 0 ? 0 : RND.nextInt(max); }
    public static int intVal(final int min, final int max) { return intVal(max - min) + min; }

    public static long longVal() { return RND.nextLong(); }

    public static String string() { return string(8); }
    public static String string(final int length) {
        return RND.ints(length, (int) '!', ((int) '~') + 1)
                .mapToObj((i) -> (char) i)
                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
                .toString()
                .trim();
    }

    public static String alphaNumeric() { return alphaNumeric(8); }
    public static String alphaNumeric(final int length) {
        return RND.ints(length, 0, ALPHA_NUM.length())
                .mapToObj(ALPHA_NUM::charAt)
                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
                .toString();
    }

    public static String numeric() { return numeric(8); }
    public static String numeric(final int length) {
        return RND.ints(length, 0, NUM.length())
                .mapToObj(NUM::charAt)
                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
                .toString();
    }

    public static boolean bool() { return RND.nextBoolean(); }

    public static UUID uuid() { return UUID.randomUUID(); }

    public static String domain() { return alphaNumeric().toLowerCase() + value(".cz", ".co.uk", ".com", ".london"); }
    public static String url() { return value("http://", "https://") + domain(); }
    public static String url(final boolean https) { return (https ? "https://" : "http://") + domain(); }

    public static String email() { return alphaNumeric() + "@" + alphaNumeric() + value(".cz", ".co.uk", ".com", ".london"); }

    public static String password() {
        return password(8);
    }

    public static String password(final CharacterType... exclude) {
        return password(8, exclude);
    }

    public static String password(final int length) {
        return password(length, CharacterType.EMPTY);
    }

    public static String password(final int length, final CharacterType... exclude) {
        final List<String> exclusions = Stream.of(exclude).map(CharacterType::getCharacters).collect(toList());
        final List<String> characterTypes = Stream.of(new CharacterType[]{LOWER_CASE, UPPER_CASE, NUMERIC, SPECIAL})
                .map(CharacterType::getCharacters).collect(toList());
        characterTypes.removeAll(exclusions);
        final String characters = characterTypes.stream().collect(joining());
        final String required = characterTypes.stream()
                .map(ch -> RND.ints(length / characterTypes.size(), 0, ch.length())
                        .mapToObj(ch::charAt).collect(StringBuilder::new, StringBuilder::append, StringBuilder::append).toString())
                .collect(joining());

        try {
            return URLEncoder.encode(required + RND.ints(length - required.length(), 0, characters.length())
                    .mapToObj(characters::charAt)
                    .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
                    .toString(), StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException uee) {
            throw new RuntimeException();
        }
    }

    @SafeVarargs
    public static <T> T value(final T... values) { return values[intVal(values.length)]; }

    @SuppressWarnings("unchecked")
    public static <T> T value(final Collection<T> values) {
        return (T) values.toArray()[intVal(values.size())];
    }

    @SuppressWarnings("unchecked")
    public static <T> Set<T> set(final T... values) {
        final Set<T> set = Stream.of(values).filter(v -> Random.bool()).collect(toSet());
        return set.isEmpty() ? Collections.singleton(values[0]) : set;
    }

    @SuppressWarnings("unchecked")
    public static <T> List<T> list(final T... values) {
        return new ArrayList<>(set(values));
    }

    public static Currency currency() { return value(Currency.getAvailableCurrencies()); }

    public static String country() { return value(COUNTRIES.keySet()); }

    public static LocalDate pastLocalDate() { return LocalDate.now().minusDays((long)intVal()); }
    public static LocalDate futureLocalDate() { return LocalDate.now().plusDays((long)intVal()); }

    public static LocalDateTime pastLocalDateTime() { return LocalDateTime.now().minusDays((long)intVal()); }
    public static LocalDateTime futureLocalDateTime() { return LocalDateTime.now().plusDays((long)intVal()); }

    public static ZonedDateTime pastZonedDateTime() { return ZonedDateTime.now(zoneId()).minusDays((long)intVal()); }
    public static ZonedDateTime futureZonedDateTime() { return ZonedDateTime.now(zoneId()).plusDays((long)intVal()); }

    public static OffsetDateTime pastOffsetDateTime() { return OffsetDateTime.now(zoneId()).minusDays((long)intVal()); }
    public static OffsetDateTime futureOffsetDateTime() { return OffsetDateTime.now(zoneId()).plusDays((long)intVal()); }

    public static Instant pastInstant() { return Instant.now().minus(intVal(), DAYS); }
    public static Instant futureInstant() { return Instant.now().plus(intVal(), DAYS); }

    public static ZoneId zoneId() { return ZoneId.of(Random.value(ZoneId.getAvailableZoneIds())); }

    public static String phone() { return '+' + numeric(intVal(7, 15)); }

    public static byte[] bytes() {
        final byte[] bytes = new byte[Random.intVal()];
        RND.nextBytes(bytes);
        return bytes;
    }

    public static String contentType() {
        return value(MediaType.GIF, MediaType.JPEG, MediaType.PDF).toString();
    }

    public static String accountNumber() {
        return accountNumber(Random.country());
    }

    public static String accountNumber(final String country) {
        return "GBR".equals(country) || "USA".equals(country) ?
                Random.numeric() : COUNTRIES.get(country) + Random.numeric(22);
    }

    public static String sortCode() { return numeric(2) + '-' + numeric(2) + '-' + numeric(2); }

    public static String routingCode() {
        return routingCode(9, true);
    }

    public static String routingCode(final boolean valid) {
        return routingCode(9, valid);
    }

    public static String routingCode(final int length, final boolean valid) {
        if (length != 9) {
            return String.valueOf(Math.abs(longVal())).substring(0, Math.min(length, 19));
        }
        final int d1 = Random.intVal(9);
        final int d2 = Random.intVal(9);
        final int d3 = Random.intVal(9);
        final int d4 = Random.intVal(9);
        final int d5 = Random.intVal(9);
        final int d6 = Random.intVal(9);
        final int d7 = Random.intVal(9);
        final int d8 = Random.intVal(9);
        final int d9 = (10 - ((d1 + d4 + d7) * 3 + (d2 + d5 + d8) * 7 + d3 + d6) % 10) % 10;

        return String.valueOf(d1) + d2 + d3 + d4 + d5 + d6 + d7 + d8
                + (valid ? d9 : String.valueOf((d9 + Random.intVal(1, 9)) % 10));
    }
}