package com.farms.service.transformer;

import com.farms.model.FieldInternal;
import com.farms.model.FieldJson;
import org.locationtech.jts.geom.Polygon;

import java.util.UUID;

/**
 * Field transformation interface. The implementation uses geotools to manipulates with Polygons.
 */
public interface FieldTransformer {

    /**
     * Transform internal field into Polygon.
     *
     * @param internal the internal field object
     * @return the Polygon object
     */
    Polygon toPolygon(FieldInternal internal);

    /**
     * Transform the internal field into external json field.
     *
     * @param internal the internal field object
     * @return the json field object
     */
    FieldJson toJson(FieldInternal internal);

    /**
     * Transform to internal field based on the fields and json field provided.
     *
     * @param farmId the farm id
     * @param fieldId the field id
     * @param json the json field object
     * @return the internal field object
     */
    FieldInternal toInternal(UUID farmId, UUID fieldId, FieldJson json);

    /**
     * Transform to internal field based on the fields.
     *
     * @param farmId the farm id
     * @param fieldId the field id
     * @param name the name
     * @param geom the geometry as Double[][][]
     * @return the internal field object
     */
    FieldInternal toInternal(UUID farmId, UUID fieldId, String name, Double[][][] geom);
}
