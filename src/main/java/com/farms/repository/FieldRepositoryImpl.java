package com.farms.repository;

import com.farms.model.FieldInternal;
import org.jooq.DSLContext;
import org.jooq.Record4;
import org.jooq.Result;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.farms.jooq.flyway.db.postgres.tables.Field.FIELD;

@Repository
public class FieldRepositoryImpl implements FieldRepository {

    private static final Logger LOG = LoggerFactory.getLogger(FieldRepositoryImpl.class);

    private static final int DEFAULT_SIZE = 10;
    private static final int DEFAULT_OFFSET = 0;

    private final DSLContext dsl;

    @Autowired
    public FieldRepositoryImpl(final DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public Optional<FieldInternal> findFieldByFarmIdAndFieldId(UUID farmId, UUID fieldId) {
        LOG.debug("Field with farm_id {} and id {} will be looked up", farmId, fieldId);

        Record4<UUID, UUID, String, byte[]> result = dsl.select(FIELD.ID, FIELD.FARM_ID, FIELD.NAME,
                DSL.field(DSL.sql("ST_AsBinary(ST_GeomFromWKB(?)) as geom", FIELD.GEOM), byte[].class))
                .from(FIELD)
                .where(FIELD.ID.equal(fieldId).and(FIELD.FARM_ID.equal(farmId)))
                .fetchAny();
        FieldInternal field = null;
        if (result != null) {
            field = result.into(FieldInternal.class);
        }
        return Optional.ofNullable(field);
    }

    @Override
    public List<FieldInternal> findFieldsByFarmId(UUID farmId, Integer size, Integer offset) {
        LOG.debug("All Fields with farm_id {} will be looked up", farmId);

        return collectFields(dsl.select(FIELD.ID, FIELD.FARM_ID, FIELD.NAME,
                DSL.field(DSL.sql("ST_AsBinary(ST_GeomFromWKB(?)) as geom", FIELD.GEOM), byte[].class))
                .from(FIELD)
                .where(FIELD.FARM_ID.equal(farmId))
                .orderBy(FIELD.NAME)
                .limit(size == null ? DEFAULT_SIZE : size)
                .offset(offset == null ? DEFAULT_OFFSET : offset)
                .fetch());
    }

    @Override
    public List<FieldInternal> findAllFields() {
        LOG.debug("All Fields will be looked up");

        return collectFields(dsl.select(FIELD.ID, FIELD.FARM_ID, FIELD.NAME,
                DSL.field(DSL.sql("ST_AsBinary(ST_GeomFromWKB(?)) as geom", FIELD.GEOM), byte[].class))
                .from(FIELD)
                .orderBy(FIELD.NAME)
                .fetch());
    }

    @Override
    public void saveField(FieldInternal field) {
        dsl.insertInto(FIELD, FIELD.ID, FIELD.FARM_ID, FIELD.NAME, FIELD.GEOM)
                .values(field.getId(), field.getFarmId(), field.getName(), field.getGeom())
                .execute();

        LOG.debug("Field with farm_id {} and field id {} has been created", field.getFarmId(), field.getId());
    }

    @Override
    public void updateField(FieldInternal field) {
        dsl.update(FIELD)
                .set(FIELD.NAME, field.getName())
                .set(FIELD.GEOM, field.getGeom())
                .where(FIELD.ID.equal(field.getId()))
                .execute();

        LOG.debug("Field with farm_id {} and field id {} has been updated", field.getFarmId(), field.getId());
    }

    @Override
    public void deleteField(UUID farmId, UUID fieldId) {
        dsl.delete(FIELD)
                .where(FIELD.ID.equal(fieldId).and(FIELD.FARM_ID.equal(farmId)))
                .execute();

        LOG.debug("Field with farm_id {} and field id {} has been deleted", farmId, fieldId);
    }

    private List<FieldInternal> collectFields(Result<Record4<UUID, UUID, String, byte[]>> result) {
        List<FieldInternal> fields = new ArrayList<>();
        if (!result.isEmpty()) {
            for (Record4<UUID, UUID, String, byte[]> record : result) {
                fields.add(record.into(FieldInternal.class));
            }
        }
        return fields;
    }
}