package com.farms;

import com.farms.error.CommonExceptionHandler;
import com.farms.error.JdbcExceptionHandler;
import com.farms.error.SecurityExceptionHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({CommonExceptionHandler.class, SecurityExceptionHandler.class, JdbcExceptionHandler.class})
public class FarmsApp extends SpringApplication {

    public static void main(final String[] args) {
        SpringApplication.run(FarmsApp.class, args);
    }
}