package com.farms.controller;

import com.farms.error.model.Errors;
import com.farms.model.Farm;
import com.farms.service.FarmService;
import com.farms.util.Random;
import org.jooq.DSLContext;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.util.UUID;

import static com.farms.jooq.flyway.db.postgres.tables.Farm.FARM;
import static com.farms.model.FarmBuilder.farmBuilder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.RequestEntity.delete;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FarmControllerDeleteITest {

    private static final String PREFIX = "/insecure";

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private DSLContext dsl;

    @Autowired
    private FarmService farmService;

    @After
    public void wipeDb() {
        dsl.delete(FARM).execute();
    }

    @Test
    public void shouldDeleteFarm() {
        Farm created = farmService.saveFarm(farmBuilder().build());

        ResponseEntity<Farm> response = restTemplate.exchange(
                delete(apiUrl("/farm", created.getId())).build(), Farm.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
        assertThat(dsl.selectCount().from(FARM).fetchAny().value1()).isEqualTo(0);
    }

    @Test
    public void shouldReturn404ForFarmNotFound() {
        UUID farmId = Random.uuid();

        ResponseEntity<Errors> response = restTemplate.exchange(
                delete(apiUrl("/farm", farmId)).build(), Errors.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    private URI apiUrl(String path, UUID farmId, String... invalidPath) {
        return URI.create("http://localhost:" + port + PREFIX + path + '/' + farmId
                + (invalidPath.length > 0 ? invalidPath[0] : ""));
    }
}