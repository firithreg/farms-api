package com.farms.service;

import com.farms.model.Farm;

import java.util.List;
import java.util.UUID;

/**
 * The Farm service
 */
public interface FarmService {

    /**
     * Find the farm by farm id.
     *
     * @param farmId the farm id
     * @return the Farm object
     */
    Farm findFarmByFarmId(UUID farmId);

    /**
     * Find all farms, paginated.
     *
     * @param size the size
     * @param offset the offset
     * @return List of Farms
     */
    List<Farm> findAllFarms(Integer size, Integer offset);

    /**
     * Save the farm.
     *
     * @param farm the farm
     * @return the saved Farm object
     */
    Farm saveFarm(Farm farm);

    /**
     * Update the farm.
     *
     * @param farm the farm
     */
    void updateFarm(Farm farm);

    /**
     * Delete the farm by farm id.
     *
     * @param farmId the farm id
     */
    void deleteFarm(UUID farmId);
}
