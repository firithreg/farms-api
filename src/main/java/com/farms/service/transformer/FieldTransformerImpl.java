package com.farms.service.transformer;

import com.farms.model.FieldInternal;
import com.farms.model.FieldJson;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.geom.impl.CoordinateArraySequence;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKBReader;
import org.locationtech.jts.io.WKBWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

@Component
public class FieldTransformerImpl implements FieldTransformer {

    private static final Logger LOG = LoggerFactory.getLogger(FieldTransformerImpl.class);

    private static final GeometryFactory GEOMETRY_FACTORY = JTSFactoryFinder.getGeometryFactory(null);
    private static final WKBReader WKB_READER = new WKBReader(GEOMETRY_FACTORY);
    private static final WKBWriter WKB_WRITER = new WKBWriter();

    @Override
    public Polygon toPolygon(FieldInternal internal) {
        if (internal != null) {
            try {
                return (Polygon) WKB_READER.read(internal.getGeom());
            } catch (ParseException pe) {
                LOG.error("Input field is not a valid field");
            }
        }
        return null;
    }

    @Override
    public FieldJson toJson(FieldInternal internal) {
        Polygon geometry = toPolygon(internal);
        Double[][] exteriorRing =
                Arrays.stream(geometry.getExteriorRing().getCoordinates())
                        .map(er -> new Double[]{er.x, er.y}).toArray(Double[][]::new);

        List<Double[][]> interiorRings = IntStream.range(0, geometry.getNumInteriorRing())
                .mapToObj(ir -> Arrays.stream(geometry.getInteriorRingN(ir).getCoordinates())
                        .map(er -> new Double[]{er.x, er.y}).toArray(Double[][]::new))
                .collect(toList());

        Double[][][] coordinates = new Double[][][]{exteriorRing};
        if (!interiorRings.isEmpty()) {
            List<Double[][]> allRings = new ArrayList<>();
            allRings.add(exteriorRing);
            allRings.addAll(interiorRings);
            coordinates = allRings.toArray(new Double[0][][]);
        }

        return new FieldJson(internal.getId(), internal.getFarmId(), internal.getName(), coordinates);
    }

    @Override
    public FieldInternal toInternal(UUID farmId, UUID fieldId, FieldJson json) {
        return toInternal(farmId, fieldId, json.getName(), json.getGeom());
    }

    @Override
    public FieldInternal toInternal(UUID farmId, UUID fieldId, String name, Double[][][] coordinates) {
        if (coordinates.length > 0) {
            List<List<Coordinate>> polygon = new ArrayList<>();
            List<Coordinate> exteriorRing = Arrays.stream(coordinates[0])
                    .map(c -> new Coordinate(c[0], c[1])).collect(toList());
            polygon.add(exteriorRing);
            List<List<Coordinate>> interiorRings;
            if (coordinates.length > 1) {
                interiorRings = IntStream.range(1, coordinates.length)
                        .mapToObj(i -> Arrays.stream(coordinates[i]).map(c -> new Coordinate(c[0], c[1])).collect(toList()))
                        .collect(Collectors.toList());
                polygon.addAll(interiorRings);
            }
            try {
                return new FieldInternal(fieldId, farmId, name,
                        WKB_WRITER.write(new Polygon(new LinearRing(new CoordinateArraySequence(polygon.get(0).stream()
                                .map(p -> new Coordinate(p.x, p.y)).toArray(Coordinate[]::new)), GEOMETRY_FACTORY),
                                IntStream.range(1, polygon.size()).mapToObj(i -> new LinearRing(new CoordinateArraySequence(
                                        polygon.get(i).stream().map(p -> new Coordinate(p.x, p.y)).toArray(Coordinate[]::new)),
                                        GEOMETRY_FACTORY)).toArray(LinearRing[]::new),
                                GEOMETRY_FACTORY)));
            } catch (Exception e) {
                LOG.error("Input field is not a valid field");
            }
        }

        return null;
    }
}