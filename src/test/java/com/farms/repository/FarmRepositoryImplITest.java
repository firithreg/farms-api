package com.farms.repository;

import com.farms.model.Farm;
import com.farms.util.Random;
import org.jooq.DSLContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.farms.jooq.flyway.db.postgres.tables.Farm.FARM;
import static com.farms.model.FarmBuilder.farmBuilder;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@RepositoryTest(FarmRepositoryImpl.class)
public class FarmRepositoryImplITest {

    @Autowired
    private FarmRepository farmRepository;

    @Autowired
    private DSLContext dsl;

    @Before
    @After
    public void wipeDb() {
        dsl.delete(FARM).execute();
    }

    @Test
    public void shouldInsertFarm() {
        UUID farmId = Random.uuid();
        Farm expectedObject = farmBuilder().id(farmId).build();

        farmRepository.saveFarm(expectedObject);

        Optional<Farm> actualObject = farmRepository.findFarmByFarmId(farmId);
        assertThat(actualObject.isPresent()).isTrue();
        assertThat(actualObject.get()).isEqualTo(expectedObject);
    }

    @Test
    public void shouldUpdateFarm() {
        UUID farmId = Random.uuid();
        Farm originalObject = farmBuilder().id(farmId).build();
        Farm updatedObject = farmBuilder()
                .id(originalObject.getId())
                .name(originalObject.getName())
                .note(originalObject.getNote())
                .build();

        farmRepository.saveFarm(originalObject);
        farmRepository.updateFarm(updatedObject);

        Optional<Farm> actualObject = farmRepository.findFarmByFarmId(updatedObject.getId());
        assertThat(actualObject.isPresent()).isTrue();
        assertThat(actualObject.get()).isEqualTo(updatedObject);
    }

    @Test
    public void shouldInsertSelectAll() {
        UUID farmId = Random.uuid();
        Farm expectedObject = farmBuilder().id(farmId).build();

        farmRepository.saveFarm(expectedObject);

        List<Farm> actualObjects = farmRepository.findAllFarms(10, 0);
        assertThat(actualObjects.size()).isEqualTo(1);
        assertThat(actualObjects).containsOnly(expectedObject);
        assertThat(actualObjects.get(0).toString()).contains(expectedObject.toString());
    }

    @Test
    public void shouldDeleteFarm() {
        UUID farmId = Random.uuid();
        Farm expectedObject = farmBuilder().id(farmId).build();

        farmRepository.saveFarm(expectedObject);
        farmRepository.deleteFarm(expectedObject.getId());

        List<Farm> actualObjects = farmRepository.findAllFarms(10, 0);
        assertThat(actualObjects.isEmpty()).isTrue();
    }
}