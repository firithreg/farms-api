-- V3__Create_Tables.sql

CREATE TABLE IF NOT EXISTS flyway_farms.farm (
  id       UUID          NOT NULL PRIMARY KEY,
  name     VARCHAR(63)   NOT NULL,
  note     VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS flyway_farms.field (
  id       UUID          NOT NULL PRIMARY KEY,
  farm_id  UUID          NOT NULL,
  name     VARCHAR(63)   NOT NULL,
  geom     GEOMETRY      NOT NULL,

  FOREIGN KEY (farm_id)  REFERENCES flyway_farms.farm (id) ON DELETE CASCADE
);
