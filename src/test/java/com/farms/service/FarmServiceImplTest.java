package com.farms.service;

import com.farms.error.NotFoundException;
import com.farms.model.Farm;
import com.farms.repository.FarmRepository;
import com.farms.util.Random;
import com.google.common.collect.ImmutableList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;
import java.util.UUID;

import static com.farms.model.FarmBuilder.farmBuilder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class FarmServiceImplTest {

    @Mock
    private FarmRepository repository;

    @InjectMocks
    private FarmServiceImpl service;

    @Test
    public void shouldCreateFarm() {
        UUID id = Random.uuid();
        Farm farm = farmBuilder().id(id).build();

        doNothing().when(repository).saveFarm(any(Farm.class));

        service.saveFarm(farm);

        verify(repository).saveFarm(any(Farm.class));
    }

    @Test
    public void shouldRetrieveFarm() {
        UUID id = Random.uuid();
        Farm farm = farmBuilder().id(id).build();

        given(repository.findFarmByFarmId(id)).willReturn(Optional.of(farm));

        assertThat(service.findFarmByFarmId(id)).isEqualTo(farm);

        verify(repository).findFarmByFarmId(id);
    }

    @Test
    public void shouldRetrieveAllFarms() {
        UUID id = Random.uuid();
        Farm farm = farmBuilder().id(id).build();

        given(repository.findAllFarms(10, 0)).willReturn(ImmutableList.of(farm));

        assertThat(service.findAllFarms(10, 0)).containsOnly(farm);

        verify(repository).findAllFarms(10, 0);
    }

    @Test
    public void shouldUpdateFarmWithName() {
        UUID id = Random.uuid();
        Farm farmOriginal = farmBuilder().id(id).build();
        Farm farmUpdated = farmBuilder().id(id).id(farmOriginal.getId()).build();

        given(repository.findFarmByFarmId(id)).willReturn(Optional.of(farmOriginal));
        doNothing().when(repository).updateFarm(farmUpdated);

        service.updateFarm(farmUpdated);

        verify(repository).updateFarm(farmUpdated);
    }

    @Test
    public void shouldDeleteFarm() {
        UUID id = Random.uuid();
        Farm farm = farmBuilder().id(id).build();

        given(repository.findFarmByFarmId(id)).willReturn(Optional.of(farm));
        doNothing().when(repository).deleteFarm(id);

        service.deleteFarm(id);

        verify(repository).deleteFarm(id);
    }

    @Test
    public void shouldNotRetrieveFarm() {
        UUID id = Random.uuid();

        given(repository.findFarmByFarmId(id)).willReturn(Optional.empty());

        assertThatThrownBy(() -> service.findFarmByFarmId(id)).isInstanceOf(NotFoundException.class);
    }

    @Test
    public void shouldNotUpdateFarm() {
        Farm farm = farmBuilder().build();

        given(repository.findFarmByFarmId(farm.getId())).willReturn(Optional.empty());

        assertThatThrownBy(() -> service.deleteFarm(farm.getId())).isInstanceOf(NotFoundException.class);
    }

    @Test
    public void shouldNotDeleteFarm() {
        UUID id = Random.uuid();

        given(repository.findFarmByFarmId(id)).willReturn(Optional.empty());

        assertThatThrownBy(() -> service.deleteFarm(id)).isInstanceOf(NotFoundException.class);
    }
}