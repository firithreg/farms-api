package com.farms.error;

import com.farms.error.model.Errors;
import com.farms.error.model.RequestError;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AccountStatusException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static java.util.Collections.singletonList;

@RestControllerAdvice
@Order(100)
@ConditionalOnClass(AccessDeniedException.class)
public class SecurityExceptionHandler {

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public Errors handleAccessDenied(final AccessDeniedException exception) {
        return new Errors(singletonList(new RequestError(exception.getMessage())));
    }

    @ExceptionHandler(AccountStatusException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public Errors handleAccountStatusException(final AccountStatusException exception) {
        return new Errors(singletonList(new RequestError(exception.getMessage())));
    }

    @ExceptionHandler(BadCredentialsException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public Errors handleBadCredentials() {
        return new Errors(singletonList(new RequestError("Bad credentials")));
    }
}
