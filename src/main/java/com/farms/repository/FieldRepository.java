package com.farms.repository;

import com.farms.model.FieldInternal;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Field repository interface
 */
public interface FieldRepository {

    /**
     * Find field by farm id and field id.
     *
     * @param farmId the farm id
     * @param fieldId the field id
     * @return Optional wrapper with Field object
     */
    Optional<FieldInternal> findFieldByFarmIdAndFieldId(UUID farmId, UUID fieldId);

    /**
     * Find fields by farm id.
     *
     * @param farmId the farm id
     * @param size the size of the returned objects
     * @param offset the offset of the returned objects
     * @return List of field objects
     */
    List<FieldInternal> findFieldsByFarmId(UUID farmId, Integer size, Integer offset);

    /**
     * Find all fields.
     *
     * @return List of field objects
     */
    List<FieldInternal> findAllFields();

    /**
     * Save the Field object.
     *
     * @param field the field object
     */
    void saveField(FieldInternal field);

    /**
     * Update the Field object.
     *
     * @param field the field object
     */
    void updateField(FieldInternal field);

    /**
     * Delete the field by farm id and field id.
     *
     * @param farmId the farm id
     * @param fieldId the field id
     */
    void deleteField(UUID farmId, UUID fieldId);
}