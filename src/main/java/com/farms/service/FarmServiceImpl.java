package com.farms.service;

import com.farms.error.NotFoundException;
import com.farms.model.Farm;
import com.farms.repository.FarmRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.lang.String.format;

@Service
public class FarmServiceImpl implements FarmService {

    private static final Logger LOG = LoggerFactory.getLogger(FarmServiceImpl.class);

    private static final String FARM_NOT_FOUND_MESSAGE = "Farm with farm_id %s has not been found in repository";

    private final FarmRepository repository;

    @Autowired
    public FarmServiceImpl(final FarmRepository repository) {
        this.repository = repository;
    }

    @Override
    public Farm findFarmByFarmId(UUID farmId) {
        LOG.info("Farm with farm_id {} will be looked up in repository", farmId);

        Optional<Farm> farm = repository.findFarmByFarmId(farmId);
        if (farm.isPresent()) {
            return farm.get();
        }

        LOG.error("Farm with farm_id {} has not been found in repository", farmId);
        throw new NotFoundException(format(FARM_NOT_FOUND_MESSAGE, farmId));
    }

    @Override
    public List<Farm> findAllFarms(Integer size, Integer offset) {
        LOG.info("All Farms will be looked up in repository");

        return repository.findAllFarms(size, offset);
    }

    @Override
    public Farm saveFarm(Farm farm) {
        Farm farmCreated = new Farm(UUID.randomUUID(), farm.getName(), farm.getNote());
        repository.saveFarm(farmCreated);

        LOG.info("Farm with farm_id {} has been created", farmCreated.getId());

        return farmCreated;
    }

    @Override
    public void updateFarm(Farm farm) {
        if (repository.findFarmByFarmId(farm.getId()).isPresent()) {
            Farm farmUpdated = new Farm(farm.getId(), farm.getName(), farm.getNote());
            repository.updateFarm(farmUpdated);

            LOG.info("Farm with farm_id {} has been updated", farmUpdated.getId());
            return;
        }

        LOG.error("Farm with farm_id {} has not been found in repository", farm.getId());
        throw new NotFoundException(format(FARM_NOT_FOUND_MESSAGE, farm.getId()));
    }

    @Override
    public void deleteFarm(UUID farmId) {
        Optional<Farm> farm = repository.findFarmByFarmId(farmId);
        if (farm.isPresent()) {
            repository.deleteFarm(farmId);

            LOG.info("Farm with farm_id {} has been deleted from repository", farmId);
            return;
        }

        LOG.error("Farm with id {} has not been found in repository", farmId);
        throw new NotFoundException(format(FARM_NOT_FOUND_MESSAGE, farmId));
    }
}