package com.farms.error;

import com.farms.config.JacksonConfig;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class SecurityExceptionHandlerTest {
    private static final ObjectMapper OBJECT_MAPPER = new JacksonConfig().jacksonBuilder().build();

    private MockMvc mockMvc = standaloneSetup(new Controller())
            .setControllerAdvice(new SecurityExceptionHandler())
            .setMessageConverters(new MappingJackson2HttpMessageConverter(OBJECT_MAPPER))
            .build();

    @Test
    public void shouldReturn401WhenAccountIsLocked() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/test/account_locked")).andReturn().getResponse();

        assertThat(response.getStatus()).isEqualTo(401);
    }

    @Test
    public void shouldReturn403WhenNotAuthorized() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/test/not_authorized")).andReturn().getResponse();

        assertThat(response.getStatus()).isEqualTo(403);
    }

    @Test
    public void shouldReturn422ForBadCredentials() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/test/bad_credentials")).andReturn().getResponse();

        assertThat(response.getStatus()).isEqualTo(422);
    }

    @RestController
    private static class Controller {

        @GetMapping("/test/not_authorized")
        public void notAuthorized() {
            throw new AccessDeniedException("test");
        }

        @GetMapping("/test/bad_credentials")
        public void badCredentials() {
            throw new BadCredentialsException("test");
        }

        @GetMapping("/test/account_locked")
        public void accountLocked() {
            throw new LockedException("test");
        }
    }
}