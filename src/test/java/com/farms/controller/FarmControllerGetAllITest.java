package com.farms.controller;

import com.farms.error.model.Errors;
import com.farms.model.Farm;
import com.farms.service.FarmService;
import com.farms.util.Random;
import org.jooq.DSLContext;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import static com.farms.jooq.flyway.db.postgres.tables.Farm.FARM;
import static com.farms.model.FarmBuilder.farmBuilder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.RequestEntity.delete;
import static org.springframework.http.RequestEntity.get;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FarmControllerGetAllITest {

    private static final String PREFIX = "/insecure";

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private DSLContext dsl;

    @Autowired
    private FarmService farmService;

    @After
    public void wipeDb() {
        dsl.delete(FARM).execute();
    }

    @Test
    public void shouldRetrieveAllFarms() {
        Farm created1 = farmService.saveFarm(farmBuilder().build());
        Farm created2 = farmService.saveFarm(farmBuilder().build());

        ResponseEntity<List<Farm>> response = restTemplate.exchange(
                get(apiUrl("/farm", null, null)).build(),
                new ParameterizedTypeReference<List<Farm>>() {});

        assertThat(response.getBody()).containsOnly(created1, created2);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(dsl.selectCount().from(FARM).fetchAny().value1()).isEqualTo(2);
    }

    @Test
    public void shouldRetrieveNoFarm() {
        ResponseEntity<List<Farm>> response = restTemplate.exchange(
                get(apiUrl("/farm", null, null)).build(),
                new ParameterizedTypeReference<List<Farm>>() {});

        assertThat(response.getBody()).isEmpty();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(dsl.selectCount().from(FARM).fetchAny().value1()).isEqualTo(0);
    }

    @Test
    public void shouldRetrieveOneFarmForSizeOne() {
        farmService.saveFarm(farmBuilder().build());
        farmService.saveFarm(farmBuilder().build());

        ResponseEntity<List<Farm>> response = restTemplate.exchange(
                get(apiUrl("/farm", 1, null)).build(),
                new ParameterizedTypeReference<List<Farm>>() {});

        assertThat(response.getBody().size()).isEqualTo(1);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(dsl.selectCount().from(FARM).fetchAny().value1()).isEqualTo(2);
    }

    @Test
    public void shouldRetrieveOneFarmForOffsetOne() {
        farmService.saveFarm(farmBuilder().build());
        farmService.saveFarm(farmBuilder().build());

        ResponseEntity<List<Farm>> response = restTemplate.exchange(
                get(apiUrl("/farm", 1, 1)).build(),
                new ParameterizedTypeReference<List<Farm>>() {});

        assertThat(response.getBody().size()).isEqualTo(1);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(dsl.selectCount().from(FARM).fetchAny().value1()).isEqualTo(2);
    }

    private URI apiUrl(String path, Integer size, Integer offset) {
        return URI.create("http://localhost:" + port + PREFIX + path
                + '?' + "size=" + (size == null ? 10 : size) + '&' + "offset=" + (offset == null ? 0 : offset));
    }
}