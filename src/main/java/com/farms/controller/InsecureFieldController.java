package com.farms.controller;

import com.farms.model.FieldJson;
import com.farms.service.FieldService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@Validated
@RestController
@RequestMapping(path = "/insecure/farm/{farmId}/field")
@Api(tags = "Field API - Insecure")
public class InsecureFieldController {

    private FieldService fieldService;

    @Autowired
    public InsecureFieldController(final FieldService fieldService) {
        this.fieldService = fieldService;
    }

    @PostMapping(consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(CREATED)
    @ApiOperation(value = "Create Field", notes = "Create a field")
    @ApiResponses(@ApiResponse(code = 201, message = "Successfully created field", response = FieldJson.class))
    public FieldJson createField(@ApiParam(name = "farm_id", value = "The unique farm id", required = true)
                                     @PathVariable("farmId") UUID farmId,
                                 @Valid @RequestBody FieldJson json) {
        return fieldService.saveField(farmId, json);
    }

    @GetMapping(value = "/{fieldId}", produces = APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(OK)
    @ApiOperation(value = "Retrieve Field", notes = "Retrieve the field")
    @ApiResponses(@ApiResponse(code = 200, message = "Successfully retrieved field", response = FieldJson.class))
    public FieldJson getField(@ApiParam(name = "farm_id", value = "The unique farm id", required = true)
                                  @PathVariable("farmId") UUID farmId,
                              @ApiParam(name = "field_id", value = "The unique field id", required = true)
                              @PathVariable("fieldId") UUID fieldId) {
        return fieldService.findFieldByFarmIdAndFieldId(farmId, fieldId);
    }

    @GetMapping(produces = APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(OK)
    @ApiOperation(value = "Retrieve Fields", notes = "Retrieve the fields")
    @ApiResponses(@ApiResponse(code = 200, message = "Successfully retrieved fields", responseContainer = "List", response = FieldJson.class))
    public List<FieldJson> getFields(@ApiParam(name = "farm_id", value = "The unique farm id", required = true)
                                         @PathVariable("farmId") UUID farmId,
                                     @ApiParam(name = "size", value = "The size of the paginated result")
                                     @RequestParam(value = "size", required = false, defaultValue = "10") @Min(1) Integer size,
                                     @ApiParam(name = "offset", value = "The offset of the paginated result")
                                         @RequestParam(value = "offset", required = false, defaultValue = "0") @Min(0) Integer offset) {
        return fieldService.findFieldsByFarmId(farmId, size, offset);
    }

    @PutMapping(consumes = APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(OK)
    @ApiOperation(value = "Update Farm", notes = "Update the field")
    @ApiResponses(@ApiResponse(code = 200, message = "Successfully updated field"))
    public void updateField(@ApiParam(name = "farm_id", value = "The unique farm id", required = true)
                                @PathVariable("farmId") UUID farmId,
                            @Valid @RequestBody FieldJson json) {
        fieldService.updateField(farmId, json);
    }

    @DeleteMapping(value = "/{fieldId}")
    @ResponseStatus(NO_CONTENT)
    @ApiOperation(value = "Delete Farm", notes = "Delete the field")
    @ApiResponses(@ApiResponse(code = 204, message = "Successfully deleted field"))
    public void deleteField(@ApiParam(name = "farm_id", value = "The unique farm id", required = true)
                                @PathVariable("farmId") UUID farmId,
                            @ApiParam(name = "field_id", value = "The unique field id", required = true)
                            @PathVariable("fieldId") UUID fieldId) {
        fieldService.deleteField(farmId, fieldId);
    }
}