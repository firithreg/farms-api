package com.farms.swagger;

import be.joengenduvel.java.verifiers.ToStringVerifier;
import com.farms.config.swagger.SwaggerApi;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.Test;

import static com.farms.swagger.SwaggerApiBuilder.swaggerApiBuilder;

public class ModelTest {

    @Test
    public void shouldTestEqualsAndHashCode() {
        EqualsVerifier.forClass(SwaggerApi.class).verify();
    }

    @Test
    public void shouldTestToString() {
        ToStringVerifier.forClass(SwaggerApi.class).containsAllPrivateFields(swaggerApiBuilder().build());
    }
}