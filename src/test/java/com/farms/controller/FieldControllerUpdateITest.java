package com.farms.controller;

import com.farms.error.model.Errors;
import com.farms.model.Farm;
import com.farms.model.FieldJson;
import com.farms.service.FarmService;
import com.farms.service.FieldService;
import com.farms.util.Random;
import org.jooq.DSLContext;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.util.UUID;

import static com.farms.jooq.flyway.db.postgres.Tables.FARM;
import static com.farms.jooq.flyway.db.postgres.tables.Field.FIELD;
import static com.farms.model.FarmBuilder.farmBuilder;
import static com.farms.model.FieldJsonBuilder.fieldJsonBuilder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.http.RequestEntity.put;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FieldControllerUpdateITest {

    private static final String PREFIX = "/insecure";

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private DSLContext dsl;

    @Autowired
    private FarmService farmService;

    @Autowired
    private FieldService fieldService;
    
    @After
    public void wipeDb() {
        dsl.delete(FIELD).execute();
        dsl.delete(FARM).execute();
    }

    @Test
    public void shouldUpdateField() {
        Farm farm = farmService.saveFarm(farmBuilder().build());

        FieldJson created = fieldService.saveField(farm.getId(), fieldJsonBuilder().farmId(farm.getId()).build());
        FieldJson field = fieldJsonBuilder().id(created.getId()).farmId(farm.getId()).name(Random.alphaNumeric()).geom(new Double[][][]{{
                {18.3912394, 49.8499648}, {18.3912394, 49.8701377}, {18.4250078, 49.8701377}, {18.4250078, 49.8499648}, {18.3912394, 49.8499648}}})
                .build();

        ResponseEntity<Void> response = restTemplate.exchange(
                put(apiUrl("/field", farm.getId())).contentType(APPLICATION_JSON_UTF8).body(field), Void.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        assertThat(fieldService.findFieldByFarmIdAndFieldId(farm.getId(), field.getId())).isEqualTo(field);
        assertThat(dsl.selectCount().from(FIELD).fetchAny().value1()).isEqualTo(1);
        assertThat(dsl.selectCount().from(FARM).fetchAny().value1()).isEqualTo(1);
    }

    @Test
    public void shouldReturn400ForInvalidUpdateValues() {
        Farm farm = farmService.saveFarm(farmBuilder().build());

        FieldJson created = fieldService.saveField(farm.getId(), fieldJsonBuilder().build());
        FieldJson field = fieldJsonBuilder().id(created.getId()).farmId(farm.getId()).name(null).geom(null).build();

        ResponseEntity<Errors> response = restTemplate.exchange(
                put(apiUrl("/field", farm.getId())).body(field), Errors.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void shouldReturn400ForInvalidFarmCoordinates() {
        Farm farm = farmService.saveFarm(farmBuilder().build());

        FieldJson created = fieldService.saveField(farm.getId(), fieldJsonBuilder().farmId(farm.getId()).build());
        FieldJson field = fieldJsonBuilder().id(created.getId()).farmId(farm.getId()).geom(new Double[][][]{{{0D, 0D}}}).build();

        ResponseEntity<Void> response = restTemplate.exchange(
                put(apiUrl("/field", farm.getId())).contentType(APPLICATION_JSON_UTF8).body(field), Void.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

        assertThat(fieldService.findFieldByFarmIdAndFieldId(farm.getId(), field.getId())).isEqualTo(created);
        assertThat(dsl.selectCount().from(FIELD).fetchAny().value1()).isEqualTo(1);
        assertThat(dsl.selectCount().from(FARM).fetchAny().value1()).isEqualTo(1);
    }

    @Test
    public void shouldReturn404ForFieldNotFound() {
        Farm farm = farmService.saveFarm(farmBuilder().build());

        ResponseEntity<Void> response = restTemplate.exchange(
        put(apiUrl("/field", farm.getId())).contentType(APPLICATION_JSON_UTF8).body(
                fieldJsonBuilder().farmId(farm.getId()).build()), Void.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    private URI apiUrl(String path, UUID farmId) {
        return URI.create("http://localhost:" + port + PREFIX + "/farm/" + farmId + path);
    }
}