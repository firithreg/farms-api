package com.farms.repository;

import com.farms.model.Farm;
import com.farms.model.FieldInternal;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static com.farms.jooq.flyway.db.postgres.tables.Farm.FARM;
import static com.farms.jooq.flyway.db.postgres.tables.Field.FIELD;
import static com.farms.model.FarmBuilder.farmBuilder;
import static com.farms.model.FieldInternalBuilder.fieldInternalBuilder;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@RepositoryTest({FarmRepositoryImpl.class, FieldRepositoryImpl.class})
public class FieldRepositoryImplITest {

    @Autowired
    private FarmRepository farmRepository;

    @Autowired
    private FieldRepository fieldRepository;

    @Autowired
    private DSLContext dsl;

    @Before
    @After
    public void wipeDb() {
        dsl.delete(FIELD).execute();
        dsl.delete(FARM).execute();
    }

    @Test
    public void shouldInsertField() {
        Farm farm = farmBuilder().build();
        farmRepository.saveFarm(farm);

        FieldInternal expectedObject = fieldInternalBuilder().farmId(farm.getId())
                .geom((byte[]) dsl.select(DSL.field(DSL.sql(
                "ST_AsBinary(ST_GeomFromText('POLYGON((0 0,10 0,10 10,0 10,0 0),(1 1,1 2,2 2,2 1,1 1))'))")))
                .fetchAny().get(0)).build();

        fieldRepository.saveField(expectedObject);

        List<FieldInternal> actualObjects = fieldRepository.findFieldsByFarmId(expectedObject.getFarmId(), 10, 0);
        assertThat(actualObjects.size()).isEqualTo(1);
        assertThat(actualObjects).containsOnly(expectedObject);
        assertThat(actualObjects.get(0).toString()).contains(expectedObject.toString());
    }

    @Test
    public void shouldUpdateField() {
        Farm farm = farmBuilder().build();
        farmRepository.saveFarm(farm);

        FieldInternal originalObject = fieldInternalBuilder().farmId(farm.getId())
                .geom((byte[]) dsl.select(DSL.field(DSL.sql(
                        "ST_AsBinary(ST_GeomFromText('POLYGON((0 0,10 0,10 10,0 10,0 0),(1 1,1 2,2 2,2 1,1 1))'))")))
                        .fetchAny().get(0)).build();
        FieldInternal updatedObject = fieldInternalBuilder()
                .farmId(farm.getId())
                .id(originalObject.getId())
                .name(originalObject.getName())
                .geom(originalObject.getGeom())
                .build();

        fieldRepository.saveField(originalObject);
        fieldRepository.updateField(updatedObject);

        List<FieldInternal> actualObjects = fieldRepository.findFieldsByFarmId(updatedObject.getFarmId(), 10, 0);
        assertThat(actualObjects.size()).isEqualTo(1);
        assertThat(actualObjects).containsOnly(updatedObject);
        assertThat(actualObjects.get(0).toString()).contains(updatedObject.toString());
    }

    @Test
    public void shouldInsertSelectAll() {
        Farm farm = farmBuilder().build();
        farmRepository.saveFarm(farm);

        FieldInternal expectedObject = fieldInternalBuilder().farmId(farm.getId())
                .geom((byte[]) dsl.select(DSL.field(DSL.sql(
                        "ST_AsBinary(ST_GeomFromText('POLYGON((0 0,10 0,10 10,0 10,0 0),(1 1,1 2,2 2,2 1,1 1))'))")))
                        .fetchAny().get(0)).build();

        fieldRepository.saveField(expectedObject);

        List<FieldInternal> actualObjects = fieldRepository.findAllFields();
        assertThat(actualObjects.size()).isEqualTo(1);
        assertThat(actualObjects).containsOnly(expectedObject);
        assertThat(actualObjects.get(0).toString()).contains(expectedObject.toString());
    }

    @Test
    public void shouldDeleteField() {
        Farm farm = farmBuilder().build();
        farmRepository.saveFarm(farm);

        FieldInternal expectedObject = fieldInternalBuilder().farmId(farm.getId())
                .geom((byte[]) dsl.select(DSL.field(DSL.sql(
                        "ST_AsBinary(ST_GeomFromText('POLYGON((0 0,10 0,10 10,0 10,0 0),(1 1,1 2,2 2,2 1,1 1))'))")))
                        .fetchAny().get(0)).build();

        fieldRepository.saveField(expectedObject);
        fieldRepository.deleteField(expectedObject.getFarmId(), expectedObject.getId());

        List<FieldInternal> actualObjects = fieldRepository.findFieldsByFarmId(expectedObject.getFarmId(), 10, 0);
        assertThat(actualObjects.isEmpty()).isTrue();
    }
}