package com.farms.model;

import com.farms.util.Random;

import java.util.UUID;

public final class FarmBuilder {
    private UUID id = Random.uuid();
    private String name = Random.alphaNumeric(63);
    private String note = Random.alphaNumeric(255);

    private FarmBuilder() {
    }

    public static FarmBuilder farmBuilder() {
        return new FarmBuilder();
    }

    public FarmBuilder id(UUID id) {
        this.id = id;
        return this;
    }

    public FarmBuilder name(String name) {
        this.name = name;
        return this;
    }

    public FarmBuilder note(String note) {
        this.note = note;
        return this;
    }

    public Farm build() {
        return new Farm(id, name, note);
    }
}
