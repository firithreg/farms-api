package com.farms.swagger;

import com.farms.config.swagger.SwaggerApi;
import com.farms.util.Random;

import java.util.Collections;
import java.util.List;

public final class SwaggerApiBuilder {

    private boolean internal = Random.bool();
    private String basePath = Random.alphaNumeric();
    private List<String> excludedPaths = Collections.singletonList(Random.alphaNumeric());

    private SwaggerApiBuilder() {
    }

    public static SwaggerApiBuilder swaggerApiBuilder() {
        return new SwaggerApiBuilder();
    }

    public SwaggerApiBuilder internal(boolean internal) {
        this.internal = internal;
        return this;
    }

    public SwaggerApiBuilder basePath(String basePath) {
        this.basePath = basePath;
        return this;
    }

    public SwaggerApiBuilder excludedPaths(List<String> excludedPaths) {
        this.excludedPaths = excludedPaths;
        return this;
    }

    public SwaggerApi build() {
        return new SwaggerApi(internal, basePath, excludedPaths);
    }
}
