package com.farms.controller;

import com.farms.error.model.Errors;
import com.farms.error.model.RequestError;
import com.farms.model.Farm;
import com.farms.service.FarmService;
import com.farms.util.Random;
import org.jooq.DSLContext;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;

import static com.farms.jooq.flyway.db.postgres.tables.Farm.FARM;
import static com.farms.model.FarmBuilder.farmBuilder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.http.RequestEntity.put;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FarmControllerUpdateITest {

    private static final String PREFIX = "/insecure";

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private DSLContext dsl;

    @Autowired
    private FarmService farmService;

    @After
    public void wipeDb() {
        dsl.delete(FARM).execute();
    }

    @Test
    public void shouldUpdateFarm() {
        Farm created = farmService.saveFarm(farmBuilder().build());
        Farm farm = farmBuilder().id(created.getId()).name(Random.alphaNumeric()).note(null).build();

        ResponseEntity<Void> response = restTemplate.exchange(
                put(apiUrl("/farm")).contentType(APPLICATION_JSON_UTF8).body(farm), Void.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        assertThat(dsl.select(FARM.ID, FARM.NAME, FARM.NOTE).from(FARM).fetchAny().into(Farm.class)).isEqualTo(farm);
    }

    @Test
    public void shouldReturn404ForFarmNotFound() {
        ResponseEntity<Void> response = restTemplate.exchange(
                put(apiUrl("/farm")).contentType(APPLICATION_JSON_UTF8).body(farmBuilder().build()), Void.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void shouldReturn422ForInvalidNotificationUrl() {
        Farm created = farmService.saveFarm(farmBuilder().build());
        Farm farm = farmBuilder().id(created.getId()).name(null).note(null).build();

        ResponseEntity<Errors> response = restTemplate.exchange(
                put(apiUrl("/farm")).body(farm), Errors.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
        assertThat(response.getBody().getErrors().size()).isEqualTo(1);
        assertThat(response.getBody().getErrors())
                .contains(new RequestError("NotNull", "name must not be null", "name", null));
    }

    private URI apiUrl(String path) {
        return URI.create("http://localhost:" + port + PREFIX + path);
    }
}