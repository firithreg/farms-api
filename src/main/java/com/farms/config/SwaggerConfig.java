package com.farms.config;

import com.farms.config.swagger.SwaggerApi;
import com.farms.config.swagger.UniqueIdSetterPlugin;
import com.fasterxml.classmate.TypeResolver;
import com.google.common.base.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.async.DeferredResult;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newTreeMap;
import static springfox.documentation.builders.PathSelectors.ant;
import static springfox.documentation.schema.AlternateTypeRules.newRule;

@Configuration
@EnableSwagger2
@Import(value = BeanValidatorPluginsConfiguration.class)
@ConditionalOnClass(Docket.class)
@ConditionalOnProperty(name = "swagger.enabled", havingValue = "true")
public class SwaggerConfig {

    private static final String CHARS_TO_ESCAPE = "(?=[]\\[+&|!(){}^\"~*?:\\\\-])";

    @Autowired
    protected TypeResolver typeResolver;

    @Bean
    public UniqueIdSetterPlugin uniqueIdSetterPlugin(@Value("${spring.application.name}") final String applicationName) {
        return new UniqueIdSetterPlugin(applicationName);
    }

    @Bean
    public Docket farmsApi(final SwaggerApi swaggerApi) {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(excludePaths(swaggerApi))
                .build()
                .directModelSubstitute(LocalDate.class, String.class)
                .genericModelSubstitutes(ResponseEntity.class)
                .alternateTypeRules(
                        newRule(typeResolver.resolve(DeferredResult.class,
                                typeResolver.resolve(ResponseEntity.class, WildcardType.class)),
                                typeResolver.resolve(WildcardType.class)))
                .useDefaultResponseMessages(false)
                .globalResponseMessage(RequestMethod.GET, getResponseMessages())
                .globalResponseMessage(RequestMethod.POST, getResponseMessages())
                .globalResponseMessage(RequestMethod.PUT, getResponseMessages())
                .globalResponseMessage(RequestMethod.DELETE, getResponseMessages())
                .globalResponseMessage(RequestMethod.PATCH, getResponseMessages())
                .globalResponseMessage(RequestMethod.HEAD, getResponseMessages())
                .apiInfo(new ApiInfo(
                        "",
                        "",
                        "1",
                        "",
                        new Contact(null, "https://farms.com", "info@farms.com"),
                        null,
                        null,
                        new ArrayList<>()));
    }

    @Bean
    public SwaggerApi swaggerApiInternal() {
        return new SwaggerApi(true, "/", Collections.emptyList());
    }

    private List<ResponseMessage> getResponseMessages() {
        return newArrayList(
                new ResponseMessage(400, "Your request is malformed",
                        new ModelRef("Errors"), newTreeMap(), newArrayList()),
                new ResponseMessage(401, "Your API key is wrong",
                        new ModelRef("Errors"), newTreeMap(), newArrayList()),
                new ResponseMessage(403, "Your API key is correct, but you don't have access to this resource",
                        new ModelRef("Errors"), newTreeMap(), newArrayList()),
                new ResponseMessage(404, "The specified object could not be found",
                        new ModelRef("Errors"), newTreeMap(), newArrayList()),
                new ResponseMessage(405, "You tried to access a resource with an invalid method",
                        new ModelRef("Errors"), newTreeMap(), newArrayList()),
                new ResponseMessage(406, "You requested a format that isn't supported",
                        new ModelRef("Errors"), newTreeMap(), newArrayList()),
                new ResponseMessage(422, "Your request is well-structured, but some parameter value isn't valid",
                        new ModelRef("Errors"), newTreeMap(), newArrayList()),
                new ResponseMessage(429, "You're request is being rate limited",
                        new ModelRef("Errors"), newTreeMap(), newArrayList()),
                new ResponseMessage(500, "We had a problem with our server, try again later",
                        new ModelRef("Errors"), newTreeMap(), newArrayList()),
                new ResponseMessage(503, "We're temporarily offline for maintenance, please try again later",
                        new ModelRef("Errors"), newTreeMap(), newArrayList()),
                new ResponseMessage(504, "We had a problem with our server and the request timed out, try again later",
                        new ModelRef("Errors"), newTreeMap(), newArrayList())
        );
    }

    private Predicate<String> excludePaths(final SwaggerApi swaggerApi) {
        return swaggerApi == null ? ant("/**") : swaggerApi.isInternal() ? ant(swaggerApi.getBasePath() + "/**")
                : input -> input != null && input.startsWith(swaggerApi.getBasePath()) && swaggerApi.getExcludedPaths() != null
                && swaggerApi.getExcludedPaths().stream().noneMatch(p -> Pattern.compile(
                "^" + swaggerApi.getBasePath().replaceAll(CHARS_TO_ESCAPE, Matcher.quoteReplacement("\\"))
                        + ".*" + p.replaceAll(CHARS_TO_ESCAPE, Matcher.quoteReplacement("\\")) + ".*$").matcher(input).matches());
    }
}