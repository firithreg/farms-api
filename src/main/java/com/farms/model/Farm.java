package com.farms.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.google.common.base.MoreObjects;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;
import java.util.UUID;

@ApiModel(value = "Farm", description = "The farm model")
public final class Farm {

    @ApiModelProperty(notes = "The id of the farm")
    private final UUID id;

    @NotNull
    @Size(max = 63)
    @ApiModelProperty(notes = "The name of the farm")
    private final String name;

    @Size(max = 255)
    @ApiModelProperty(notes = "The note of the farm")
    private final String note;

    @JsonCreator
    public Farm(final UUID id, final String name, final String note) {
        this.id = id;
        this.name = name;
        this.note = note;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getNote() {
        return note;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Farm that = (Farm) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(note, that.note);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, note);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("note", note)
                .toString();
    }
}