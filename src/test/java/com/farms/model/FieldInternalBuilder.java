package com.farms.model;

import com.farms.util.Random;

import java.util.UUID;

public final class FieldInternalBuilder {
    private UUID id = Random.uuid();
    private UUID farmId = Random.uuid();
    private String name = Random.alphaNumeric(63);
    private byte[] geom;

    private FieldInternalBuilder() {
    }

    public static FieldInternalBuilder fieldInternalBuilder() {
        return new FieldInternalBuilder();
    }

    public FieldInternalBuilder id(UUID id) {
        this.id = id;
        return this;
    }

    public FieldInternalBuilder farmId(UUID farmId) {
        this.farmId = farmId;
        return this;
    }

    public FieldInternalBuilder name(String name) {
        this.name = name;
        return this;
    }

    public FieldInternalBuilder geom(byte[] geom) {
        this.geom = geom;
        return this;
    }

    public FieldInternal build() {
        return new FieldInternal(id, farmId, name, geom);
    }
}
