package com.farms.controller;

import com.farms.model.Farm;
import com.farms.service.FarmService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@Validated
@RestController
@RequestMapping(path = "/insecure/farm")
@Api(tags = "Farm API - Insecure")
public class InsecureFarmController {

    private FarmService farmService;

    @Autowired
    public InsecureFarmController(final FarmService farmService) {
        this.farmService = farmService;
    }

    @PostMapping(consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(CREATED)
    @ApiOperation(value = "Create Farm", notes = "Create a farm of fields")
    @ApiResponses(@ApiResponse(code = 201, message = "Successfully created farm", response = Farm.class))
    public Farm createFarm(@Valid @RequestBody Farm json) {
        return farmService.saveFarm(json);
    }

    @GetMapping(value = "/{farmId}", produces = APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(OK)
    @ApiOperation(value = "Retrieve Farm", notes = "Retrieve the farm of fields")
    @ApiResponses(@ApiResponse(code = 200, message = "Successfully retrieved farm", response = Farm.class))
    public Farm getFarm(@ApiParam(name = "farm_id", value = "The unique farm id", required = true)
                            @PathVariable("farmId") UUID farmId) {
        return farmService.findFarmByFarmId(farmId);
    }

    @GetMapping(produces = APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(OK)
    @ApiOperation(value = "Retrieve Farms", notes = "Retrieve all farms of fields, paginated")
    @ApiResponses(@ApiResponse(code = 200, message = "Successfully retrieved farms", responseContainer = "List", response = Farm.class))
    public List<Farm> getFarms(@ApiParam(name = "size", value = "The size of the paginated result")
                                   @RequestParam(value = "size", required = false, defaultValue = "10") @Min(1) Integer size,
                               @ApiParam(name = "offset", value = "The offset of the paginated result")
                               @RequestParam(value = "offset", required = false, defaultValue = "0") @Min(0) Integer offset) {
        return farmService.findAllFarms(size, offset);
    }

    @PutMapping(consumes = APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(OK)
    @ApiOperation(value = "Update Farm", notes = "Update the farm of fields")
    @ApiResponses(@ApiResponse(code = 200, message = "Successfully updated farm"))
    public void updateFarm(@Valid @RequestBody Farm json) {
        farmService.updateFarm(json);
    }

    @DeleteMapping(value = "/{farmId}")
    @ResponseStatus(NO_CONTENT)
    @ApiOperation(value = "Delete Farm", notes = "Delete the farm of fields")
    @ApiResponses(@ApiResponse(code = 204, message = "Successfully deleted farm"))
    public void deleteFarm(@ApiParam(name = "farm_id", value = "The unique farm id", required = true)
                               @PathVariable("farmId") UUID farmId) {
        farmService.deleteFarm(farmId);
    }
}