package com.farms.config;

import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.FeatureSource;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.locationtech.jts.geom.MultiPolygon;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.Collections;

@Configuration
public class BoundariesConfig {

    private static final String COUNTRY = "Czech Republic";
    private static final int COUNTRY_SELECTION_INDEX = 19;
    private static final int GEOMETRY_SELECTION_INDEX = 0;

    @Value("classpath:data/ne_10m_admin_0_countries.shp")
    private Resource resourceFile;

    @Bean
    public MultiPolygon getCzechRepublic() throws IOException {
        DataStore inputDataStore = DataStoreFinder.getDataStore(
                Collections.singletonMap("url", resourceFile.getURL()));

        String inputTypeName = inputDataStore.getTypeNames()[0];
        FeatureSource<SimpleFeatureType, SimpleFeature> source = inputDataStore.getFeatureSource(inputTypeName);
        FeatureCollection<SimpleFeatureType, SimpleFeature> inputFeatureCollection = source.getFeatures();
        MultiPolygon czechRepublic = null;
        try (FeatureIterator<SimpleFeature> featureIterator = inputFeatureCollection.features()) {
            while (featureIterator.hasNext()) {
                SimpleFeature feature = featureIterator.next();
                if (COUNTRY.equals(feature.getAttributes().get(COUNTRY_SELECTION_INDEX))) {
                    czechRepublic = (MultiPolygon) feature.getAttributes().get(GEOMETRY_SELECTION_INDEX);
                }
            }
        }
        inputDataStore.dispose();
        return czechRepublic;
    }
}