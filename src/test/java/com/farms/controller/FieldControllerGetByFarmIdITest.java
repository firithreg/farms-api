package com.farms.controller;

import com.farms.model.Farm;
import com.farms.model.FieldJson;
import com.farms.service.FarmService;
import com.farms.service.FieldService;
import org.jooq.DSLContext;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import static com.farms.jooq.flyway.db.postgres.Tables.FARM;
import static com.farms.jooq.flyway.db.postgres.tables.Field.FIELD;
import static com.farms.model.FarmBuilder.farmBuilder;
import static com.farms.model.FieldJsonBuilder.fieldJsonBuilder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.RequestEntity.get;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FieldControllerGetByFarmIdITest {

    private static final String PREFIX = "/insecure";

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private DSLContext dsl;

    @Autowired
    private FarmService farmService;

    @Autowired
    private FieldService fieldService;

    @After
    public void wipeDb() {
        dsl.delete(FIELD).execute();
        dsl.delete(FARM).execute();
    }


    @Test
    public void shouldRetrieveAllFields() {
        Farm farm = farmService.saveFarm(farmBuilder().build());

        FieldJson created = fieldService.saveField(farm.getId(), fieldJsonBuilder().farmId(farm.getId()).build());

        ResponseEntity<List<FieldJson>> response = restTemplate.exchange(
                get(apiUrl("/field", farm.getId(), null, null)).build(),
                new ParameterizedTypeReference<List<FieldJson>>() {});

        assertThat(response.getBody()).containsOnly(created);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(dsl.selectCount().from(FIELD).fetchAny().value1()).isEqualTo(1);
        assertThat(dsl.selectCount().from(FARM).fetchAny().value1()).isEqualTo(1);
    }

    @Test
    public void shouldRetrieveNoField() {
        Farm farm = farmService.saveFarm(farmBuilder().build());

        ResponseEntity<List<FieldJson>> response = restTemplate.exchange(
                get(apiUrl("/field", farm.getId(), null, null)).build(),
                new ParameterizedTypeReference<List<FieldJson>>() {});
        
        assertThat(response.getBody()).isEmpty();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(dsl.selectCount().from(FIELD).fetchAny().value1()).isEqualTo(0);
        assertThat(dsl.selectCount().from(FARM).fetchAny().value1()).isEqualTo(1);
    }

    @Test
    public void shouldRetrieveOneFieldForSizeOne() {
        Farm farm = farmService.saveFarm(farmBuilder().build());

        fieldService.saveField(farm.getId(), fieldJsonBuilder().farmId(farm.getId()).build());

        ResponseEntity<List<FieldJson>> response = restTemplate.exchange(
                get(apiUrl("/field", farm.getId(), 1, null)).build(),
                new ParameterizedTypeReference<List<FieldJson>>() {});

        assertThat(response.getBody().size()).isEqualTo(1);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(dsl.selectCount().from(FIELD).fetchAny().value1()).isEqualTo(1);
        assertThat(dsl.selectCount().from(FARM).fetchAny().value1()).isEqualTo(1);
    }

    @Test
    public void shouldRetrieveNoFieldForOffsetOne() {
        Farm farm = farmService.saveFarm(farmBuilder().build());

        fieldService.saveField(farm.getId(), fieldJsonBuilder().farmId(farm.getId()).build());

        ResponseEntity<List<FieldJson>> response = restTemplate.exchange(
                get(apiUrl("/field", farm.getId(), null, 1)).build(),
                new ParameterizedTypeReference<List<FieldJson>>() {});

        assertThat(response.getBody().size()).isEqualTo(0);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(dsl.selectCount().from(FIELD).fetchAny().value1()).isEqualTo(1);
        assertThat(dsl.selectCount().from(FARM).fetchAny().value1()).isEqualTo(1);
    }

    private URI apiUrl(String path, UUID farmId, Integer size, Integer offset) {
        return URI.create("http://localhost:" + port + PREFIX + "/farm/" + farmId + path
                + '?' + "size=" + (size == null ? 10 : size) + '&' + "offset=" + (offset == null ? 0 : offset));
    }
}