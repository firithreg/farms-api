package com.farms.service;

import com.farms.model.FieldJson;

import java.util.List;
import java.util.UUID;

/**
 * The Field service
 */
public interface FieldService {

    /**
     * Find Field by farm id and field id.
     *
     * @param farmId the farm id
     * @param fieldId the field id
     * @return Field json object
     */
    FieldJson findFieldByFarmIdAndFieldId(UUID farmId, UUID fieldId);

    /**
     * Find FieldJson objects by farm id
     *
     * @param farmId the farm id
     * @param size the size of returned objects
     * @param offset the offset of returned objects
     * @return List of Field objects
     */
    List<FieldJson> findFieldsByFarmId(UUID farmId, Integer size, Integer offset);

    /**
     * Save the FieldJson of Farm with farm id.
     *
     * @param farmId the farm id
     * @param field the field object
     * @return the saved FieldJson object
     */
    FieldJson saveField(UUID farmId, FieldJson field);

    /**
     * Update the FieldJson of Farm with farm id.
     *
     * @param farmId the farm id
     * @param field the field object
     */
    void updateField(UUID farmId, FieldJson field);

    /**
     * Delete FieldJson by farm id and field id.
     *
     * @param farmId the farm id
     * @param fieldId the field id
     */
    void deleteField(UUID farmId, UUID fieldId);
}
