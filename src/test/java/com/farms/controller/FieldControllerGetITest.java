package com.farms.controller;

import com.farms.error.model.Errors;
import com.farms.model.Farm;
import com.farms.model.FieldJson;
import com.farms.service.FarmService;
import com.farms.service.FieldService;
import com.farms.util.Random;
import org.jooq.DSLContext;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.util.UUID;

import static com.farms.jooq.flyway.db.postgres.Tables.FARM;
import static com.farms.jooq.flyway.db.postgres.tables.Field.FIELD;
import static com.farms.model.FarmBuilder.farmBuilder;
import static com.farms.model.FieldJsonBuilder.fieldJsonBuilder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.RequestEntity.get;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FieldControllerGetITest {

    private static final String PREFIX = "/insecure";

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private DSLContext dsl;

    @Autowired
    private FarmService farmService;

    @Autowired
    private FieldService fieldService;

    @After
    public void wipeDb() {
        dsl.delete(FIELD).execute();
        dsl.delete(FARM).execute();
    }

    @Test
    public void shouldGetField() {
        Farm farm = farmService.saveFarm(farmBuilder().build());

        FieldJson created = fieldService.saveField(farm.getId(), fieldJsonBuilder().build());

        ResponseEntity<FieldJson> response = restTemplate.exchange(
                get(apiUrl("/field", farm.getId(), created.getId())).build(), FieldJson.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(created);
    }

    @Test
    public void shouldReturn404ForFieldNotFound() {
        Farm farm = farmService.saveFarm(farmBuilder().build());

        ResponseEntity<Errors> response = restTemplate.exchange(
                get(apiUrl("/field", farm.getId(), Random.uuid())).build(), Errors.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    private URI apiUrl(String path, UUID farmId, UUID fieldId) {
        return URI.create("http://localhost:" + port + PREFIX + "/farm/" + farmId + path + '/' + fieldId);
    }
}