package com.farms.model;


import be.joengenduvel.java.verifiers.ToStringVerifier;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.Test;

import static com.farms.model.FarmBuilder.farmBuilder;
import static com.farms.model.FieldInternalBuilder.fieldInternalBuilder;
import static com.farms.model.FieldJsonBuilder.fieldJsonBuilder;

public class ModelTest {

    @Test
    public void shouldTestEqualsAndHashCode() {
        EqualsVerifier.forClass(Farm.class).verify();
        EqualsVerifier.forClass(FieldInternal.class).verify();
        EqualsVerifier.forClass(FieldJson.class).verify();
    }

    @Test
    public void shouldTestToString() {
        ToStringVerifier.forClass(Farm.class).containsAllPrivateFields(farmBuilder().build());
        ToStringVerifier.forClass(FieldInternal.class).ignore("geom").containsAllPrivateFields(fieldInternalBuilder().build());
        ToStringVerifier.forClass(FieldJson.class).ignore("geom").containsAllPrivateFields(fieldJsonBuilder().build());
    }
}