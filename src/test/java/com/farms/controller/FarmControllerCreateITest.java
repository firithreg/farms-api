package com.farms.controller;

import com.farms.error.model.Errors;
import com.farms.model.Farm;
import org.jooq.DSLContext;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;

import static com.farms.jooq.flyway.db.postgres.tables.Farm.FARM;
import static com.farms.model.FarmBuilder.farmBuilder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.http.RequestEntity.delete;
import static org.springframework.http.RequestEntity.post;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class FarmControllerCreateITest {

    private static final String PREFIX = "/insecure";

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private DSLContext dsl;

    @After
    public void wipeDb() {
        dsl.delete(FARM).execute();
    }

    @Test
    public void shouldCreateFarm() {
        Farm farm = farmBuilder().build();
        ResponseEntity<Farm> response = restTemplate.exchange(
                post(apiUrl("/farm")).contentType(APPLICATION_JSON_UTF8).body(farm), Farm.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        Farm json = response.getBody();
        assertThat(json.getId()).isNotNull();
        assertThat(json.getName()).isEqualTo(farm.getName());
        assertThat(json.getNote()).isEqualTo(farm.getNote());

        assertThat(dsl.selectCount().from(FARM).fetchAny().value1()).isEqualTo(1);
    }

    @Test
    public void shouldReturn405ForForInvalidMethod() {
        ResponseEntity<Errors> response = restTemplate.exchange(
                delete(apiUrl("/farm")).build(), Errors.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.METHOD_NOT_ALLOWED);
    }

    private URI apiUrl(String path) {
        return URI.create("http://localhost:" + port + PREFIX + path);
    }
}