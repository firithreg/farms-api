package com.farms.service;

import com.farms.error.NotFoundException;
import com.farms.model.Farm;
import com.farms.model.FieldInternal;
import com.farms.model.FieldJson;
import com.farms.repository.FarmRepository;
import com.farms.repository.FieldRepository;
import com.farms.service.transformer.FieldTransformer;
import com.farms.util.Random;
import com.google.common.collect.ImmutableList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.locationtech.jts.geom.MultiPolygon;
import org.locationtech.jts.geom.Polygon;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import static com.farms.model.FarmBuilder.farmBuilder;
import static com.farms.model.FieldInternalBuilder.fieldInternalBuilder;
import static com.farms.model.FieldJsonBuilder.fieldJsonBuilder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class FieldServiceImplTest {

    @Mock
    private FarmRepository farmRepository;
    
    @Mock
    private FieldRepository fieldRepository;

    @Mock
    private FieldTransformer transformer;

    @Mock
    private MultiPolygon boundaries;

    @Mock
    private Polygon polygon;

    @InjectMocks
    private FieldServiceImpl service;

    @Test
    public void shouldCreateField() {
        UUID farmId = Random.uuid();
        Farm farm = farmBuilder().id(farmId).build();
        FieldJson field = fieldJsonBuilder().farmId(farmId).build();
        FieldInternal fieldInternal = fieldInternalBuilder().farmId(farmId).id(field.getId()).build();

        given(transformer.toInternal(eq(farmId), any(UUID.class), eq(field))).willReturn(fieldInternal);
        given(transformer.toJson(fieldInternal)).willReturn(field);
        doNothing().when(fieldRepository).saveField(fieldInternal);
        given(transformer.toPolygon(fieldInternal)).willReturn(polygon);
        given(polygon.isValid()).willReturn(true);
        given(polygon.getArea()).willReturn(123D);
        given(boundaries.disjoint(polygon)).willReturn(false);
        given(farmRepository.findFarmByFarmId(farmId)).willReturn(Optional.of(farm));
        given(fieldRepository.findAllFields()).willReturn(Collections.emptyList());

        service.saveField(farmId, field);

        verify(fieldRepository).saveField(fieldInternal);
    }

    @Test
    public void shouldRetrieveField() {
        UUID farmId = Random.uuid();
        FieldJson field = fieldJsonBuilder().farmId(farmId).build();
        FieldInternal fieldInternal = fieldInternalBuilder().farmId(farmId).id(field.getId()).build();

        given(transformer.toJson(fieldInternal)).willReturn(field);
        given(fieldRepository.findFieldByFarmIdAndFieldId(farmId, field.getId())).willReturn(Optional.of(fieldInternal));

        assertThat(service.findFieldByFarmIdAndFieldId(farmId, field.getId())).isEqualTo(field);

        verify(fieldRepository).findFieldByFarmIdAndFieldId(farmId, field.getId());
    }

    @Test
    public void shouldRetrieveFieldsByFarmId() {
        UUID farmId = Random.uuid();
        FieldJson field = fieldJsonBuilder().farmId(farmId).build();
        FieldInternal fieldInternal = fieldInternalBuilder().farmId(farmId).id(field.getId()).build();

        given(transformer.toJson(fieldInternal)).willReturn(field);
        given(fieldRepository.findFieldsByFarmId(farmId, 10, 0)).willReturn(ImmutableList.of(fieldInternal));

        assertThat(service.findFieldsByFarmId(farmId, 10, 0)).containsOnly(field);

        verify(fieldRepository).findFieldsByFarmId(farmId, 10, 0);
    }

    @Test
    public void shouldUpdateFieldWithName() {
        UUID farmId = Random.uuid();
        FieldJson field = fieldJsonBuilder().farmId(farmId).geom(null).build();
        FieldJson fieldOriginal = fieldJsonBuilder().id(field.getId()).farmId(farmId).build();
        FieldInternal fieldInternal = fieldInternalBuilder().farmId(farmId).id(field.getId()).build();

        given(fieldRepository.findFieldByFarmIdAndFieldId(farmId, field.getId())).willReturn(Optional.of(fieldInternal));
        given(transformer.toInternal(farmId, field.getId(), field.getName(), fieldOriginal.getGeom())).willReturn(fieldInternal);
        given(transformer.toJson(fieldInternal)).willReturn(fieldOriginal);
        doNothing().when(fieldRepository).updateField(fieldInternal);

        service.updateField(farmId, field);

        verify(fieldRepository).updateField(fieldInternal);
    }

    @Test
    public void shouldUpdateFieldWithGeom() {
        UUID farmId = Random.uuid();
        FieldJson field = fieldJsonBuilder().farmId(farmId).name(null).build();
        FieldJson fieldOriginal = fieldJsonBuilder().id(field.getId()).farmId(farmId).build();
        FieldInternal fieldInternal = fieldInternalBuilder().farmId(farmId).id(field.getId()).build();

        given(fieldRepository.findFieldByFarmIdAndFieldId(farmId, field.getId())).willReturn(Optional.of(fieldInternal));
        given(transformer.toInternal(farmId, field.getId(), fieldOriginal.getName(), field.getGeom())).willReturn(fieldInternal);
        given(transformer.toJson(fieldInternal)).willReturn(fieldOriginal);
        given(transformer.toPolygon(fieldInternal)).willReturn(polygon);
        given(polygon.isValid()).willReturn(true);
        given(polygon.getArea()).willReturn(123D);
        given(boundaries.disjoint(polygon)).willReturn(false);
        doNothing().when(fieldRepository).updateField(fieldInternal);

        service.updateField(farmId, field);

        verify(fieldRepository).updateField(fieldInternal);
    }

    @Test
    public void shouldDeleteField() {
        UUID farmId = Random.uuid();
        FieldJson field = fieldJsonBuilder().farmId(farmId).build();
        FieldInternal fieldInternal = fieldInternalBuilder().farmId(farmId).id(field.getId()).build();

        given(fieldRepository.findFieldByFarmIdAndFieldId(farmId, field.getId())).willReturn(Optional.of(fieldInternal));
        doNothing().when(fieldRepository).deleteField(farmId, field.getId());

        service.deleteField(farmId, field.getId());

        verify(fieldRepository).deleteField(farmId, field.getId());
    }

    @Test
    public void shouldNotRetrieveField() {
        UUID farmId = Random.uuid();
        UUID fieldId = Random.uuid();

        given(fieldRepository.findFieldByFarmIdAndFieldId(farmId, fieldId)).willReturn(Optional.empty());

        assertThatThrownBy(() -> service.findFieldByFarmIdAndFieldId(farmId, fieldId))
                .isInstanceOf(NotFoundException.class);
    }
}