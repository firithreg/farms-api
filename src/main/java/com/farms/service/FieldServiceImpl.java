package com.farms.service;

import com.farms.error.BadRequestException;
import com.farms.error.NotFoundException;
import com.farms.model.FieldInternal;
import com.farms.model.FieldJson;
import com.farms.repository.FarmRepository;
import com.farms.repository.FieldRepository;
import com.farms.service.transformer.FieldTransformer;
import org.locationtech.jts.geom.MultiPolygon;
import org.locationtech.jts.geom.Polygon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.IntStream;

import static java.lang.String.format;

@Service
public class FieldServiceImpl implements FieldService {

    private static final Logger LOG = LoggerFactory.getLogger(FieldServiceImpl.class);

    private static final String INCONSISTENT_FIELD_MESSAGE = "Inconsistent data for farm_id %s and field_id %s in database";
    private static final String INCONSISTENT_FARM_MESSAGE = "Inconsistent data for farm_id %s in database";
    private static final String FIELD_NOT_FOUND_MESSAGE = "Field for farm_id %s with field_id %s is not found";
    private static final String FARM_NOT_FOUND_MESSAGE = "Farm with farm_id %s has not been found in repository";
    private static final String FIELDS_IN_FARM_NOT_FOUND_MESSAGE = "Fields with farm_id %s has not been found in repository";
    private static final String INCORRECT_FIELD_MESSAGE = "Field has incorrect input values";
    private static final String INCORRECT_COORDINATES_MESSAGE = "Field has incorrect coordinates";
    private static final String INVALID_FIELD_MESSAGE = "Field name and geom for farm_id %s may not be null";
    private static final String INVALID_BOUNDARIES_MESSAGE = "Field is out of the selected country";
    private static final String INVALID_POLYGON_MESSAGE = "Field is not a valid polygon";
    private static final String INVALID_AREA_MESSAGE = "Field cannot have the area size 0";
    private static final String INTERSECTION_MESSAGE = "Field intersects existing fields in the database";

    private final FarmRepository farmRepository;
    private final FieldRepository fieldRepository;
    private final FieldTransformer transformer;
    private final MultiPolygon boundaries;

    @Autowired
    public FieldServiceImpl(final FarmRepository farmRepository, final FieldRepository fieldRepository,
                            final FieldTransformer transformer, final MultiPolygon boundaries) {
        this.farmRepository = farmRepository;
        this.fieldRepository = fieldRepository;
        this.transformer = transformer;
        this.boundaries = boundaries;
    }

    @Override
    public FieldJson findFieldByFarmIdAndFieldId(UUID farmId, UUID fieldId) {
        LOG.info("Field with farm_id {} and id {} will be looked up in fieldRepository", farmId, fieldId);

        Optional<FieldInternal> fieldInternal = fieldRepository.findFieldByFarmIdAndFieldId(farmId, fieldId);
        if (fieldInternal.isPresent()) {
            try {
               return transformer.toJson(fieldInternal.get());
            } catch (Exception e) {
                throw new BadRequestException(format(INCONSISTENT_FIELD_MESSAGE, farmId, fieldId));
            }
        }

        LOG.error("Field with farm_id {} and field_id {} has not been found in fieldRepository", farmId, fieldId);
        throw new NotFoundException(format(FIELD_NOT_FOUND_MESSAGE, farmId, fieldId));
    }

    @Override
    public List<FieldJson> findFieldsByFarmId(UUID farmId, Integer size, Integer offset) {
        LOG.info("All Fields with farm_id {} will be looked up in fieldRepository", farmId);

        List<FieldInternal> fieldInternals = fieldRepository.findFieldsByFarmId(farmId, size, offset);
        try {
            List<FieldJson> fieldJsons = new ArrayList<>();
            for (FieldInternal field : fieldInternals) {
                fieldJsons.add(transformer.toJson(field));
            }
            return fieldJsons;
        } catch (Exception e) {
            LOG.error("Fields with farm_id {} are found corrupted in fieldRepository", farmId);
            throw new BadRequestException(format(INCONSISTENT_FARM_MESSAGE, farmId));
        }
    }

    @Override
    public FieldJson saveField(UUID farmId, FieldJson field) {
        if (field.getName() != null && field.getGeom() != null) {
            if (farmRepository.findFarmByFarmId(farmId).isPresent()) {
                FieldInternal fieldInternal = transformer.toInternal(farmId, UUID.randomUUID(), field);
                Polygon fieldAsPolygon = transformer.toPolygon(fieldInternal);
                validateFields(fieldAsPolygon);
                fieldRepository.saveField(fieldInternal);
                LOG.info("Field with farm_id {} and field id {} has been created in fieldRepository", field.getFarmId(), field.getId());

                return transformer.toJson(fieldInternal);
            } else {
                LOG.error("Field with farm_id {} has not been created", farmId);
                throw new BadRequestException(format(FARM_NOT_FOUND_MESSAGE, farmId));
            }
        }

        LOG.error("Field with farm_id {} has not been created", farmId);
        throw new BadRequestException(format(INVALID_FIELD_MESSAGE, farmId));
    }

    @Override
    public void updateField(UUID farmId, FieldJson field) {
        Optional<FieldInternal> fieldInternalOriginal = fieldRepository.findFieldByFarmIdAndFieldId(farmId, field.getId());
        if (fieldInternalOriginal.isPresent()) {
            FieldJson fieldOriginal = transformer.toJson(fieldInternalOriginal.get());
            String name = field.getName();
            Double[][][] coordinates = field.getGeom();
            if (name == null && coordinates == null) {
                throw new BadRequestException(INCORRECT_FIELD_MESSAGE);
            }
            if (name == null) {
                name = fieldOriginal.getName();
            }
            if (coordinates == null) {
                coordinates = fieldOriginal.getGeom();
                FieldInternal fieldInternal = transformer.toInternal(farmId, field.getId(), name, coordinates);
                fieldRepository.updateField(fieldInternal);

                LOG.info("Field with farm_id {} and field id {} has been updated in fieldRepository", farmId, field.getId());
                return;
            }
            FieldInternal fieldInternal = transformer.toInternal(farmId, field.getId(), name, coordinates);
            Polygon fieldAsPolygon = transformer.toPolygon(fieldInternal);
            Polygon fieldOriginalAsPolygon = transformer.toPolygon(fieldInternalOriginal.get());
            validateFields(fieldAsPolygon, fieldOriginalAsPolygon);
            fieldRepository.updateField(fieldInternal);

            LOG.info("Field with farm_id {} and field id {} has been updated in fieldRepository", farmId, field.getId());
            return;
        }

        LOG.error("Fields with farm_id {} has not been found in fieldRepository", farmId);
        throw new NotFoundException(format(FIELDS_IN_FARM_NOT_FOUND_MESSAGE, farmId));
    }

    @Override
    public void deleteField(UUID farmId, UUID fieldId) {
        Optional<FieldInternal> fieldInternal = fieldRepository.findFieldByFarmIdAndFieldId(farmId, fieldId);
        if (fieldInternal.isPresent()) {
            fieldRepository.deleteField(farmId, fieldId);

            LOG.info("Field with farm_id {} and with id {} has been deleted from fieldRepository", farmId, fieldId);
            return;
        }

        LOG.error("Field with id {} has not been found in fieldRepository", fieldId);
        throw new NotFoundException(format(FIELD_NOT_FOUND_MESSAGE, farmId, fieldId));
    }

    private void validateFields(Polygon... polygons) {
        if (polygons[0] != null) {
            if (!polygons[0].isValid()) {
                throw new BadRequestException(INVALID_POLYGON_MESSAGE);
            }
            if (polygons[0].getArea() == 0D) {
                throw new BadRequestException(INVALID_AREA_MESSAGE);
            }
            if (boundaries.disjoint(polygons[0])) {
                throw new BadRequestException(INVALID_BOUNDARIES_MESSAGE);
            }
            if (fieldRepository.findAllFields().stream().anyMatch(
                    f -> {
                        Polygon polygon = transformer.toPolygon(f);
                        return IntStream.range(1, polygons.length).noneMatch(p -> polygon.equals(polygons[p]))
                                && polygon.intersects(polygons[0]);
                    })) {
                throw new BadRequestException(INTERSECTION_MESSAGE);
            }
        } else {
            throw new BadRequestException(INCORRECT_COORDINATES_MESSAGE);
        }
    }
}
