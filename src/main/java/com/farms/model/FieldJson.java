package com.farms.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.google.common.base.MoreObjects;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Size;
import java.util.Arrays;
import java.util.Objects;
import java.util.UUID;

@ApiModel(value = "Field", description = "The field model")
public final class FieldJson {

    @ApiModelProperty(notes = "The id of the field")
    private final UUID id;

    @ApiModelProperty(notes = "The id of the farm")
    private final UUID farmId;

    @Size(max = 63)
    @ApiModelProperty(notes = "The name of the field")
    private final String name;

    @ApiModelProperty(notes = "The geometry coordinates of the polygon field with holes. The first array of coordinates define external shape, the following internal holes")
    private final Double[][][] geom;

    @JsonCreator
    public FieldJson(final UUID id, final UUID farmId, final String name, final Double[][][] geom) {
        this.id = id;
        this.farmId = farmId;
        this.name = name;
        this.geom = geom;
    }

    public UUID getId() {
        return id;
    }

    public UUID getFarmId() {
        return farmId;
    }

    public String getName() {
        return name;
    }

    public Double[][][] getGeom() {
        return geom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FieldJson fieldJson = (FieldJson) o;
        return Objects.equals(id, fieldJson.id) &&
                Objects.equals(farmId, fieldJson.farmId) &&
                Objects.equals(name, fieldJson.name) &&
                Arrays.deepEquals(geom, fieldJson.geom);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, farmId, name);
        result = 31 * result + Arrays.deepHashCode(geom);
        return result;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("farmId", farmId)
                .add("name", name)
                .add("geom", geom)
                .toString();
    }
}