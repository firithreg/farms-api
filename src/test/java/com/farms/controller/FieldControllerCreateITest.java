package com.farms.controller;

import com.farms.error.model.Errors;
import com.farms.model.Farm;
import com.farms.model.FieldJson;
import com.farms.service.FarmService;
import com.farms.util.Random;
import org.jooq.DSLContext;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.util.UUID;

import static com.farms.jooq.flyway.db.postgres.Tables.FARM;
import static com.farms.jooq.flyway.db.postgres.tables.Field.FIELD;
import static com.farms.model.FarmBuilder.farmBuilder;
import static com.farms.model.FieldJsonBuilder.fieldJsonBuilder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.http.RequestEntity.delete;
import static org.springframework.http.RequestEntity.post;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class FieldControllerCreateITest {

    private static final String PREFIX = "/insecure";

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private DSLContext dsl;

    @Autowired
    private FarmService farmService;

    @After
    public void wipeDb() {
        dsl.delete(FIELD).execute();
        dsl.delete(FARM).execute();
    }

    @Test
    public void shouldCreateField() {
        Farm farm = farmService.saveFarm(farmBuilder().build());

        FieldJson field = fieldJsonBuilder().farmId(farm.getId()).build();
        ResponseEntity<FieldJson> response = restTemplate.exchange(
                post(apiUrl("/field", farm.getId())).contentType(APPLICATION_JSON_UTF8).body(field), FieldJson.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        FieldJson json = response.getBody();
        assertThat(json.getId()).isNotNull();
        assertThat(json.getName()).isEqualTo(field.getName());
        assertThat(json.getGeom()).isEqualTo(field.getGeom());

        assertThat(dsl.selectCount().from(FIELD).fetchAny().value1()).isEqualTo(1);
    }

    @Test
    public void shouldReturn400ForInvalidFarm() {
        Farm farm = farmService.saveFarm(farmBuilder().build());

        FieldJson field = fieldJsonBuilder().farmId(farm.getId()).geom(
                new Double[][][]{{{0D, 0D}}}).build();
        ResponseEntity<Errors> response = restTemplate.exchange(
                post(apiUrl("/field", farm.getId())).contentType(APPLICATION_JSON_UTF8).body(field), Errors.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void shouldReturn400ForFarmIntersection() {
        Farm farm = farmService.saveFarm(farmBuilder().build());

        FieldJson created = fieldJsonBuilder().farmId(farm.getId()).build();

        restTemplate.exchange(post(apiUrl("/field", farm.getId()))
                .contentType(APPLICATION_JSON_UTF8).body(created), Errors.class);

        FieldJson field = fieldJsonBuilder().farmId(farm.getId()).geom(
                new Double[][][]{{{0D, 0D}}}).build();

        ResponseEntity<Errors> response = restTemplate.exchange(
                post(apiUrl("/field", farm.getId())).contentType(APPLICATION_JSON_UTF8).body(field), Errors.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(dsl.selectCount().from(FIELD).fetchAny().value1()).isEqualTo(1);
        assertThat(dsl.selectCount().from(FARM).fetchAny().value1()).isEqualTo(1);
    }

    @Test
    public void shouldReturn405ForForInvalidMethod() {
        ResponseEntity<Errors> response = restTemplate.exchange(
                delete(apiUrl("/field", Random.uuid())).build(), Errors.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.METHOD_NOT_ALLOWED);
    }

    private URI apiUrl(String path, UUID farmId) {
        return URI.create("http://localhost:" + port + PREFIX + "/farm/" + farmId + path);
    }
}