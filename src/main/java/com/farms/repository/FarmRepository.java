package com.farms.repository;

import com.farms.model.Farm;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Farm repository interface
 */
public interface FarmRepository {

    /**
     * Find farm by id.
     * @param farmId the farm id
     * @return Optional wrapper with Farm object
     */
    Optional<Farm> findFarmByFarmId(UUID farmId);

    /**
     * Find all farms.
     * @return List of Farm objects
     */
    List<Farm> findAllFarms(Integer size, Integer offset);

    /**
     * Save a farm.
     * @param farm the Farm object
     */
    void saveFarm(Farm farm);

    /**
     * Update a farm.
     * @param farm the Farm object
     */
    void updateFarm(Farm farm);

    /**
     * Delete the farm by id.
     * @param farmId the farm id
     */
    void deleteFarm(UUID farmId);
}