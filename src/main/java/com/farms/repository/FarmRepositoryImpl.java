package com.farms.repository;

import com.farms.model.Farm;
import org.jooq.DSLContext;
import org.jooq.Record3;
import org.jooq.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.farms.jooq.flyway.db.postgres.tables.Farm.FARM;

@Repository
public class FarmRepositoryImpl implements FarmRepository {

    private static final Logger LOG = LoggerFactory.getLogger(FarmRepositoryImpl.class);

    private static final int DEFAULT_SIZE = 10;
    private static final int DEFAULT_OFFSET = 0;

    private final DSLContext dsl;

    @Autowired
    public FarmRepositoryImpl(final DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public Optional<Farm> findFarmByFarmId(UUID farmId) {
        LOG.debug("Farm with farm_id {} will be looked up", farmId);

        Record3<UUID, String, String> result = dsl.select(FARM.ID, FARM.NAME, FARM.NOTE)
                .from(FARM)
                .where(FARM.ID.equal(farmId))
                .fetchAny();
        Farm farm = null;
        if (result != null) {
            farm = result.into(Farm.class);
        }
        return Optional.ofNullable(farm);
    }

    @Override
    public List<Farm> findAllFarms(Integer size, Integer offset) {
        LOG.debug("All Farms with will be looked up");

        Result<Record3<UUID, String, String>> result = dsl.select(FARM.ID, FARM.NAME, FARM.NOTE)
                .from(FARM)
                .orderBy(FARM.NAME)
                .limit(size == null ? DEFAULT_SIZE : size)
                .offset(offset == null ? DEFAULT_OFFSET : offset)
                .fetch();

        List<Farm> farms = new ArrayList<>();
        if (!result.isEmpty()) {
            for (Record3<UUID, String, String> record : result) {
                farms.add(record.into(Farm.class));
            }
        }
        return farms;
    }

    @Override
    public void saveFarm(Farm farm) {
        dsl.insertInto(FARM, FARM.ID, FARM.NAME, FARM.NOTE)
                .values(farm.getId(), farm.getName(), farm.getNote())
                .execute();

        LOG.debug("Farm with farm_id {} has been created", farm.getId());
    }

    @Override
    public void updateFarm(Farm farm) {
        dsl.update(FARM)
                .set(FARM.NAME, farm.getName())
                .set(FARM.NOTE, farm.getNote())
                .where(FARM.ID.equal(farm.getId()))
                .execute();

        LOG.debug("Farm with farm_id {} has been updated", farm.getId());
    }

    @Override
    public void deleteFarm(UUID farmId) {
        dsl.delete(FARM)
                .where(FARM.ID.equal(farmId))
                .execute();

        LOG.debug("Farm with farm_id {} has been deleted", farmId);
    }
}