package com.farms.config.swagger;

import com.google.common.base.Optional;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.OperationBuilderPlugin;
import springfox.documentation.spi.service.contexts.OperationContext;
import springfox.documentation.swagger.common.SwaggerPluginSupport;

public class UniqueIdSetterPlugin implements OperationBuilderPlugin {

    private final String operationIdPrefix;

    public UniqueIdSetterPlugin(final String operationIdPrefix) {
        this.operationIdPrefix = operationIdPrefix;
    }

    @Override
    public void apply(final OperationContext context) {
        final Optional<ApiOperation> methodAnnotation = context.findAnnotation(ApiOperation.class);
        if (methodAnnotation.isPresent()) {
            context.operationBuilder().uniqueId(operationIdPrefix + "_" + context.getName());
        }
    }

    @Override
    public boolean supports(final DocumentationType delimiter) {
        return SwaggerPluginSupport.pluginDoesApply(delimiter);
    }
}
