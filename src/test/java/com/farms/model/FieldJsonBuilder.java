package com.farms.model;

import com.farms.util.Random;

import java.util.UUID;

public final class FieldJsonBuilder {
    private UUID id = Random.uuid();
    private UUID farmId = Random.uuid();
    private String name = Random.alphaNumeric(63);
    private Double[][][] geom = new Double[][][]{{
            {18.3912394, 49.8499648}, {18.3912394, 49.8701377}, {18.4250078, 49.8701377}, {18.4250078, 49.8499648}, {18.3912394, 49.8499648}},
            {{18.4012394, 49.8599648}, {18.4012394, 49.8601377}, {18.4150078, 49.8601377}, {18.4150078, 49.8599648}, {18.4012394, 49.8599648}}};
    private FieldJsonBuilder() {
    }

    public static FieldJsonBuilder fieldJsonBuilder() {
        return new FieldJsonBuilder();
    }

    public FieldJsonBuilder id(UUID id) {
        this.id = id;
        return this;
    }

    public FieldJsonBuilder farmId(UUID farmId) {
        this.farmId = farmId;
        return this;
    }

    public FieldJsonBuilder name(String name) {
        this.name = name;
        return this;
    }

    public FieldJsonBuilder geom(Double[][][] geom) {
        this.geom = geom;
        return this;
    }

    public FieldJson build() {
        return new FieldJson(id, farmId, name, geom);
    }
}
