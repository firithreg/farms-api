package com.farms.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.google.common.base.MoreObjects;

import java.util.Arrays;
import java.util.Objects;
import java.util.UUID;

public final class FieldInternal {

    private final UUID id;

    private final UUID farmId;

    private final String name;

    private final byte[] geom;

    @JsonCreator
    public FieldInternal(final UUID id, final UUID farmId, final String name, final byte[] geom) {
        this.id = id;
        this.farmId = farmId;
        this.name = name;
        this.geom = geom;
    }

    public UUID getId() {
        return id;
    }

    public UUID getFarmId() {
        return farmId;
    }

    public String getName() {
        return name;
    }

    public byte[] getGeom() {
        return geom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FieldInternal that = (FieldInternal) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(farmId, that.farmId) &&
                Objects.equals(name, that.name) &&
                Arrays.equals(geom, that.geom);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, farmId, name);
        result = 31 * result + Arrays.hashCode(geom);
        return result;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("farmId", farmId)
                .add("name", name)
                .add("geom", geom)
                .toString();
    }
}